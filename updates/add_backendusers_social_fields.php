<?php namespace jammerxd\backendusers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BackendUsersAddSocialFields extends Migration
{

    public function up()
    {
        if (!Schema::hasColumn('backend_users', 'about')) {
            Schema::table('backend_users', function($table)
			{
				$table->text('about')->nullable();
			});
        }

        if (!Schema::hasColumn('backend_users', 'social_twitter')) {
            Schema::table('backend_users', function($table)
			{
				$table->string('social_twitter')->nullable();
			});
        }

        if (!Schema::hasColumn('backend_users', 'social_twitch')) {
            Schema::table('backend_users', function($table)
			{
				$table->string('social_twitch')->nullable();
			});
        }
        if (!Schema::hasColumn('backend_users', 'social_instagram')) {
            Schema::table('backend_users', function($table)
			{
				$table->string('social_instagram')->nullable();
			});
        }
        if (!Schema::hasColumn('backend_users', 'social_youtube')) {
			Schema::table('backend_users', function($table)
			{
				$table->string('social_youtube')->nullable();
			});
        }

       
    }

    public function down()
    {
    }

}