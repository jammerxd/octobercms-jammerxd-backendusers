<?php namespace jammerxd\backendusers\Models;

use Model;


/**
 * Model
 */
class User extends Model
{



    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    /*public $rules = [
        'social_twitter' => 'required',

    ];

    public $customMessages = [
                'social_twitter.required' => 'Twitter account is required',




    ];*/

    /**
     * @var string The database table used by the model.
     */
    public $table = 'backend_users';
}