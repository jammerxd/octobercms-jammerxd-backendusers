<?php 
namespace jammerxd\backendusers;

use System\Classes\PluginBase;
use Backend\Models\User as BackendUserModel;
use Backend\Controllers\Users as BackendUsersController;
class Plugin extends PluginBase
{
	
	public function pluginDetails()
    {
        return [
            'name'        => 'backendusers',
            'description' => 'adds social fields to the backend_users table',
            'author'      => 'Josh Menzel',
            'icon'        => 'icon-pencil',
            'homepage'    => ''
        ];
    }
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
          // Add college and teacher field to Users table
        //BackendUserModel::extend(function($model){
        //    $model->belongsTo['team'] = ['technobrave\team\Models\Team'];            

//        });


		//BackendUserModel::extend(function($model){
		//	$myrules = $model->rules;
		//	$myrules['social_twitter'] = 'required';
		//	$model->rules = $myrules;
		//});
	
         // Add college and teacher field to Users form
         BackendUsersController::extendFormFields(function($form, $model, $context){

            if (!$model instanceof BackendUserModel)
                return;

            $form->addTabFields([
                'social_twitter' => [
                    'label'   => 'Twitter',
                    'type' => 'text'
                ],
				'social_twitch' => [
                    'label'   => 'Twitch',
                    'type' => 'text'
                ],
				'social_youtube' => [
                    'label'   => 'Youtube Channel',
                    'type' => 'text'
                ],
				'social_instagram' => [
                    'label'   => 'Instagram',
                    'type' => 'text'
                ],
				'about' => [
                    'label'   => 'Bio',
                    'type' => 'textarea'
                ],
            ]);
        });
    }
}